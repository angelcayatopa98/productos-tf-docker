provider "azurerm" {
  features {}
  client_id       = "ebb7c04e-c082-4d70-a917-946527c711a2"
  client_secret   = "ifT8Q~TTwJGWhIReTTRu9ELZ1a3Po2eS8TYXnb9-"
  subscription_id = "1f4972fb-5fbf-44cf-b2c6-52e66b6778b6"
  tenant_id       = "1337ba66-60cd-4358-a228-d9ed50c8c575"
}

resource "azurerm_resource_group" "producto_resource_group" {
  name     = "producto_resource_group"
  location = "East US"
}

resource "azurerm_virtual_network" "producto_virtual_network" {
  name                = "producto_virtual_network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.producto_resource_group.location
  resource_group_name = azurerm_resource_group.producto_resource_group.name
}

resource "azurerm_subnet" "producto_subnet" {
  name                 = "producto_subnet"
  resource_group_name  = azurerm_resource_group.producto_resource_group.name
  virtual_network_name = azurerm_virtual_network.producto_virtual_network.name
  address_prefixes     = ["10.0.0.0/24"]  # Ajusta la dirección IP según tus necesidades
}

resource "azurerm_linux_virtual_machine" "productovm" {
  name                  = "productovm"
  resource_group_name   = azurerm_resource_group.producto_resource_group.name
  location              = azurerm_resource_group.producto_resource_group.location
  size                  = "Standard_DS2_v2"
  admin_username        = "producto_admin"
  admin_password        = "admin123!"  # ¡Cambia esto por una contraseña segura!
  disable_password_authentication = false
  network_interface_ids = [azurerm_network_interface.producto_ni.id]
  os_disk {
    caching = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts"
    version   = "latest"
  }
}

resource "azurerm_virtual_machine_extension" "producto_vme" {
  name                 = "producto_vme"
  virtual_machine_id   = azurerm_linux_virtual_machine.productovm.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"

  settings = <<SETTINGS
 {
  "commandToExecute": "sudo apt-get update"
 }
 SETTINGS
}

resource "azurerm_public_ip" "producto_public_ip" {
  name                = "producto_public_ip"
  location            = azurerm_resource_group.producto_resource_group.location
  resource_group_name = azurerm_resource_group.producto_resource_group.name
  allocation_method   = "Dynamic"  # Puedes cambiar a "Static" si lo deseas

}

resource "azurerm_network_security_group" "producto_nsg" {
  name                = "producto_nsg"
  location            = azurerm_resource_group.producto_resource_group.location
  resource_group_name = azurerm_resource_group.producto_resource_group.name
}

resource "azurerm_network_security_rule" "producto_ssh" {
  name                        = "producto_ssh"
  priority                    = 1001
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = azurerm_public_ip.producto_public_ip.ip_address  # Reemplaza por tu dirección IP pública
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.producto_resource_group.name
  network_security_group_name = azurerm_network_security_group.producto_nsg.name
}

resource "azurerm_network_interface" "producto_ni" {
  name                = "producto_ni"
  location            = azurerm_resource_group.producto_resource_group.location
  resource_group_name = azurerm_resource_group.producto_resource_group.name
  ip_configuration {
    name                          = "producto-config"
    subnet_id                     = azurerm_subnet.producto_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.producto_public_ip.id
  }
}


